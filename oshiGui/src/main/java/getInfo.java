import oshi.SystemInfo;
import oshi.hardware.*;
import oshi.software.os.FileSystem;
import oshi.software.os.NetworkParams;

import java.util.Arrays;
import java.util.List;

/*
g5cc 2021-04-01
*/

public class getInfo {
    public String getInfo() {
        SystemInfo systemInfo = new SystemInfo(); //系统信息
        HardwareAbstractionLayer hal = systemInfo.getHardware(); //硬件信息
        ComputerSystem computerSystem = hal.getComputerSystem(); //计算机系统
        CentralProcessor centralProcessor = hal.getProcessor(); //处理吕信息
        GlobalMemory globalMemory = hal.getMemory(); //全局内存信息
        Sensors sensors = hal.getSensors();//传感器信息
        PowerSource[] powerSource = hal.getPowerSources().toArray(new PowerSource[0]);//电源信息
        HWDiskStore[] hwDiskStore = hal.getDiskStores().toArray(new HWDiskStore[0]);//硬盘信息
        FileSystem fileSystem = systemInfo.getOperatingSystem().getFileSystem();//文件系统
        NetworkIF[] networkIF = hal.getNetworkIFs().toArray(new NetworkIF[0]);//网卡信息
        //NetworkParams[] networkParams = hal.getNetworkIFs().toArray(new NetworkIF[0]);//网络参数
        Display[] display = hal.getDisplays().toArray(new Display[0]);//显示信息
        //UsbDevice[] usbDevice = hal.getUsbDevices(true).toArray(new UsbDevice[0]);//usb信息
        StringBuilder sb = new StringBuilder();

        sb.append("操作系统：" + systemInfo.getOperatingSystem() + "\n");
        sb.append("制造商：" + computerSystem.getManufacturer() + "\n");
        sb.append("型号：" + computerSystem.getModel() + "\n");
        sb.append("序列号：" + computerSystem.getSerialNumber() + "\n");
        sb.append("主板型号：" + computerSystem.getBaseboard().getModel() + "\n");
        sb.append("CPU型号：" + centralProcessor.getProcessorIdentifier().getName() + "\n");
        sb.append("数量：" + centralProcessor.getPhysicalPackageCount() + " | ");
        sb.append("物理核心：" + centralProcessor.getPhysicalProcessorCount() + " | ");
        sb.append("逻辑核心：" + centralProcessor.getLogicalProcessorCount() + "\n");
        sb.append("可用内存：" + globalMemory.getAvailable() / 1024 / 1024 + " MB / ");
        sb.append("总内存：" + globalMemory.getTotal() / 1024 / 1024 + " MB\n");

        //物理内存信息
        List<PhysicalMemory> memoryList = globalMemory.getPhysicalMemory();
        for (PhysicalMemory ram : memoryList) {
            sb.append("内存物理信息=" + " ");
            sb.append("厂商：" + ram.getManufacturer() + " ");
            sb.append("大小：" + ram.getCapacity() / 1024 / 1024 + " MB ");
            sb.append("类型：" + ram.getMemoryType() + "\n");
        }

        //电源信息
        for (PowerSource pw : powerSource) {

            sb.append("电源信息=");
            sb.append("制造商：" + pw.getManufacturer() + " | ");
            sb.append("当前容量：" + pw.getCurrentCapacity() + "\n");
            sb.append("当前电量：" + String.format("%n %s / %.1f%%", pw.getName(), pw.getRemainingCapacityPercent() * 100d) + "\n");
        }

        //硬盘信息
        for (HWDiskStore disk : hwDiskStore) {
            sb.append("硬盘信息：" + disk.getModel() + "\n");
        }

        //网卡信息
        for (NetworkIF anIf : networkIF) {
            sb.append("网卡信息：" + anIf.getDisplayName() + "\n");
        }

        //显示信息
        for (Display dis : display) {
            sb.append("显示信息：" + dis.toString() + "\n");
        }

        //生成硬件特征码
        String vendor = computerSystem.getManufacturer();
        String processorSerialNumber = computerSystem.getSerialNumber();
        String uuid = computerSystem.getHardwareUUID();
        String processorIdentifier = centralProcessor.getProcessorIdentifier().getIdentifier();
        int processors = centralProcessor.getLogicalProcessorCount();
        String delimiter = "-";

        String hardcode= String.format("%08x", vendor.hashCode()) + delimiter
            + String.format("%08x", processorSerialNumber.hashCode()) + delimiter
            + String.format("%08x", uuid.hashCode()) + delimiter
            + String.format("%08x", processorIdentifier.hashCode()) + delimiter + processors;

        sb.append("硬件特征码："+hardcode+" 用于注册码识别");

        return sb.toString();
    }
}

