import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ihardwaerinfo extends JFrame{
    public static void main(String[] args) {
        JFrame jFrame=new JFrame("电脑硬件信息获取-测试用");

        jFrame.setBounds(600,200,500,600);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JTextArea txt=new JTextArea();
        JLabel time=new JLabel();

        JButton btn=new JButton("获取信息");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                long startTime = System.currentTimeMillis(); //获取开始时间
                txt.setText(new getInfo().getInfo());
                long endTime = System.currentTimeMillis(); //获取结束时间
                time.setText(String.valueOf(endTime-startTime)+" ms");
            }
        });

        jFrame.setLayout(null);

        txt.setBounds(5,5,475,500);
        btn.setBounds(200,520,100,30);
        time.setBounds(430,520,60,15);
        Container container=jFrame.getContentPane();
        container.add(txt);
        container.add(btn);
        container.add(time);

        jFrame.setResizable(false);
        jFrame.setVisible(true);

    }
}
